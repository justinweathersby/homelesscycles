Rails.application.routes.draw do

  devise_for :users, path_names: {sign_in: "login", sign_out: "logout"},
                     controllers: {:registraions => "users/registrations"}
                     
  root 'static_pages#index'

  get '5k_race' => 'static_pages#race_5k', :as => "race_5k"
end
