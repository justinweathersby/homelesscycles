class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


 validates_uniqueness_of :email
 validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
 validates :name, presence: true
 validates :age, presence: true, numericality: {greater_than: 0}
 validates :phone, presence: true

 # def age(dob)
 #  now = Time.now.utc.to_date
 #  now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
 # end

end
