#--Overides Devise's Failer redirect. So When a user is not authenticated they ActiveRecord
#--Sent back to the login page

class CustomFailure < Devise::FailureApp
  def redirect_url
    new_user_session_path()
  end

  def respond
    if http_auth?
      http_auth
    else
      redirect
    end
  end
end
